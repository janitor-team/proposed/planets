planets (0.1.13-20) unstable; urgency=medium

  * Team upload
  * Fix compilation with OCaml 4.08.0

 -- Stéphane Glondu <glondu@debian.org>  Sat, 07 Sep 2019 20:47:14 +0200

planets (0.1.13-19) unstable; urgency=medium

  * Make package arch=any (closes: #898173)
  * Standards-Version 4.1.4 (no change)
  * d/copyright: use https in format specification
  * Set Vcs-{Git,Browser} to salsa addresses

 -- Ralf Treinen <treinen@debian.org>  Tue, 08 May 2018 21:58:18 +0200

planets (0.1.13-18) unstable; urgency=medium

  * Depends: use ${ocaml:Depends}, drop static dependency entries
  * debian/rules: use dh --with ocaml
  * Build-dependencies:
    - Drop build-dependency on tk8.5-dev, as we have tk-dev
    - Merge Build-Depends-Indep into Build-Depends as there is only one
      binary package.
    - Drop version constraint in build-dependency dh-ocaml
  * Debhelper compatibility level 11
  * Standards-Version 4.1.3:
    - use https in Vcs-*

 -- Ralf Treinen <treinen@debian.org>  Sat, 20 Jan 2018 10:23:08 +0100

planets (0.1.13-17) unstable; urgency=medium

  * debhelper compat level 10
  * Standards-version 4.1.0:
    - remove debian/menu since there is an upstream desktop file
    - change Priority to optional
  * improve wording in long description
  * fix spelling in debian/doc-base.planets-intro

 -- Ralf Treinen <treinen@debian.org>  Tue, 26 Sep 2017 08:18:45 +0200

planets (0.1.13-16) unstable; urgency=medium

  * Team upload.
  * Fix installability on bytecode architectures.

 -- Mehdi Dogguy <mehdi@debian.org>  Fri, 16 Oct 2015 21:20:22 +0200

planets (0.1.13-15) unstable; urgency=low

  * Team upload
  * Fix bug number in previous changelog entry
  * Add liblabltk-ocaml-dev to Build-Depends

 -- Stéphane Glondu <glondu@debian.org>  Sat, 10 Oct 2015 07:20:14 +0200

planets (0.1.13-14) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Fix compilation with OCaml 4.01.0 (Closes: #731273)
  * Add tk-dev to Build-Depends
  * Update Vcs-*

  [ Ralf Treinen ]
  * Standards-Version 3.9.5 (no change)
  * Recompile with ocaml 4.01.0 (no source change)
  * debian/copyright: convert to machine-readable format 1.0

 -- Stéphane Glondu <glondu@debian.org>  Sat, 07 Dec 2013 09:17:24 +0100

planets (0.1.13-13) unstable; urgency=low

  * Team upload
  * Recompile with ocaml 3.12.1 (no source changes)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 01 Nov 2011 21:26:53 +0100

planets (0.1.13-12) unstable; urgency=low

  [ Ralf Treinen ]
  * debian/rules: migrate to dh, bump version of debhelper
  * Standards-version: 3.9.2 (no change)
  * Use dh-ocaml to compute dependency on ocaml runtime (closes: #599327)
  * Bump versions on dh-ocaml and ocaml-nox

  [ Stéphane Glondu ]
  * Switch packaging to git

 -- Ralf Treinen <treinen@debian.org>  Sun, 17 Apr 2011 20:29:58 +0200

planets (0.1.13-11) unstable; urgency=low

  * modified debian/patches/fix_desktop: added Science to desktop entry
    categories list. Thanks to Petter Reinholdtsen for the patch.
    (Closes: #576497).
  * Removed Zack from uploaders on his request.

 -- Ralf Treinen <treinen@debian.org>  Tue, 22 Jun 2010 20:01:09 +0200

planets (0.1.13-10) unstable; urgency=low

  * Convert to source format 3.0 (quilt)
    - debian/paches quilt style
    - no more patching in debian/rules
    - added debian/source/format
  * Standards-version 3.8.4 (no change)
  * Recompile for ocaml 3.11.2

 -- Ralf Treinen <treinen@debian.org>  Mon, 08 Feb 2010 21:35:07 +0100

planets (0.1.13-9) unstable; urgency=low

  * debian/control: added Homepage
  * Standards-Version: 3.8.2 (no change)
  * Rebuild for ocaml 3.11.1

 -- Ralf Treinen <treinen@debian.org>  Wed, 24 Jun 2009 23:05:16 +0200

planets (0.1.13-8) unstable; urgency=low

  [ Stephane Glondu ]
  * Remove Julien from Uploaders

  [ Ralf Treinen ]
  * Add build-dependency on dh-ocaml
  * Relaxed build-dependency on ocaml to >= 3.04
  * Removed spurious dependency on ocaml-base | ocaml
  * Add dependency on ${misc:Depends}
  * debian/rules, cdbs: class/ocaml.mk -> rules/ocaml.mk
  * debian/rules: use variable $(DEB_DESTDIR) for install commands

 -- Ralf Treinen <treinen@debian.org>  Sat, 07 Mar 2009 21:04:45 +0100

planets (0.1.13-7) unstable; urgency=low

  * debian/control: Bump Standards-Version (no changes necessary).
  * debian/control: Switch from Tk 8.4 to Tk 8.5.
  * debian/control: Add dependency ocaml-base | ocaml, to provide a real
    package alternative to ocaml-base-$ABI, and satisfy lintian.

 -- Martin Pitt <mpitt@debian.org>  Sat, 06 Sep 2008 10:59:43 +0200

planets (0.1.13-6) unstable; urgency=low

  * Added watch file.
  * Recompile for ocaml 3.10.2
  * Clean up uploaders, added myself

 -- Ralf Treinen <treinen@debian.org>  Mon, 19 May 2008 22:02:06 +0200

planets (0.1.13-5) unstable; urgency=low

  * Convert to cdbs.
  * Split out formerly inline patches to debian/patches/, using
    simple-patchsys.mk.
  * debian/copyright: Add actual copyright declaration.
  * Add Vcs-{Svn,Browser} fields.
  * debian/control: Fix build dependencies.

 -- Martin Pitt <mpitt@debian.org>  Tue, 25 Mar 2008 18:35:51 +0100

planets (0.1.13-4) unstable; urgency=medium

  * Rebuilt against OCaml 3.10.1
  * Bump urgency to medium for the OCaml transition
  * Bump standards-version, no changes needed

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 02 Mar 2008 14:46:33 +0100

planets (0.1.13-3) unstable; urgency=low

  * Ocaml 3.10 is in unstable, upload previous experimental version to
    unstable, too.

 -- Martin Pitt <mpitt@debian.org>  Sun, 16 Sep 2007 13:39:12 +0200

planets (0.1.13-2) experimental; urgency=low

  * Build against ocaml 3.10 in experimental.
  * debian/control: Build-Depend on camlp4, it is a separate package now.
    (Closes: #441507)

 -- Martin Pitt <mpitt@debian.org>  Sun, 29 Jul 2007 12:05:52 +0200

planets (0.1.13-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Move debhelper from B-D-I to B-D so that clean always
    works.
  * debian/copyright: Update FSF address, fix reference to GPL version ("2 or
    later", not "2 only").
  * debian/control: Set maintainer to Debian OCaml Maintainers team.

 -- Martin Pitt <mpitt@debian.org>  Sun, 22 Jul 2007 14:00:50 +0200

planets (0.1.12-7) unstable; urgency=low

  * Build against the current ocaml ABI.
  * Bump Standards-Version to 3.7.2.
  * Install existing desktop file and icon. Thanks to Barry deFreese
    <bddebian@comcast.net>!

 -- Martin Pitt <mpitt@debian.org>  Sun, 21 May 2006 13:03:28 +0200

planets (0.1.12-6) unstable; urgency=low

  * debian/rules: Build-depend against unversioned ocaml.
  * debian/rules, debian/control: Determine ocaml version at build time and
    use a variable instead of hardcoding the ocaml version. Thanks to Julien
    Cristau for the patch!

 -- Martin Pitt <mpitt@debian.org>  Fri, 13 Jan 2006 18:47:55 +0100

planets (0.1.12-5) unstable; urgency=low

  * debian/control: Update dependencies to ocaml 3.09.0.

 -- Martin Pitt <mpitt@debian.org>  Thu, 17 Nov 2005 19:48:04 +0100

planets (0.1.12-4) unstable; urgency=high

  * debian/control: Update dependencies to ocaml 3.08.3. (closes: #304161)
  * Urgency high since this fixes an RC bug and no source changes were made.

 -- Martin Pitt <mpitt@debian.org>  Tue, 12 Apr 2005 12:39:58 +0200

planets (0.1.12-3) unstable; urgency=low

  * recompiled with and changed dependencies to ocaml 3.08 (closes: #262357)
  * quoted all values in menu file

 -- Martin Pitt <mpitt@debian.org>  Sun,  1 Aug 2004 20:59:23 +0200

planets (0.1.12-2) unstable; urgency=low

  * added German translation
  * added README.Debian with forum link
  * doc-base entries, menu: consistent section Games/Simulation
  * updated my maintainer address

 -- Martin Pitt <mpitt@debian.org>  Mon, 24 Nov 2003 00:30:04 +0100

planets (0.1.12-1) unstable; urgency=low

  * new upstream version which works with ocaml 3.07
  * updated dependencies to work with ocaml 3.07 and tk 8.4
  * updated to Standards-Version 3.6.1
  * debian/rules now respects DEB_BUILD_OPTIONS
  * moved menu entry to new section Games/Simulation

 -- Martin Pitt <martin@piware.de>  Fri,  3 Oct 2003 01:22:19 +0200

planets (0.1.10-3) unstable; urgency=low

  * lstrings.ml: fixed program crash with LANG=C (closes: #194402)

 -- Martin Pitt <martin@piware.de>  Fri, 23 May 2003 13:58:52 +0200

planets (0.1.10-2) unstable; urgency=low

  * 0.1.10-1 was rejected because the alternative package with a natively
    compiled version on supporting architectures was regarded confusing
  * updated initial release (closes: #187988)
  * removed native package planets, renamed planets-byte to planets
  * removed README.Debian, since it only talked about native/bytecode versions

 -- Martin Pitt <martin@piware.de>  Wed, 14 May 2003 10:04:14 +0200

planets (0.1.10-1) unstable; urgency=low

  * Initial release (closes: #187988)

 -- Martin Pitt <martin@piware.de>  Thu,  1 May 2003 22:25:33 +0200
